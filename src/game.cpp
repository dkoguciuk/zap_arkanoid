#include "game.h"

#define WIDTH       ((int)320)
#define HEIGHT      ((int)240)
#define STEP        ((int)4)

Game::Game()
{
    // Create playground
    playground = cv::Mat(HEIGHT, WIDTH, CV_8UC1);

    // Vars
    left = false;
    up = false;

    x = 3*WIDTH/4;
    y = 1*HEIGHT/3;
}

void Game::simulate()
{
    // Bouce it!
    if (y<0) up = false;
    if (y>HEIGHT) up = true;
    if (x<0) left = false;
    if (x>WIDTH) left = true;

    // Move it!
    if (left) x-=STEP;
    else x+=STEP;
    if (up) y-=STEP;
    else y+=STEP;
}

QImage Game::getGameScene()
{
    // Make playground black!
    playground.setTo(0);

    // Draw my ball!
    cv::circle(playground, cv::Point(x,y), 10, cv::Scalar(255), -1);

    // Convert to QImage
    return QImage((uchar*) playground.data, playground.cols, playground.rows, playground.step, QImage::Format_Indexed8).copy();
}
