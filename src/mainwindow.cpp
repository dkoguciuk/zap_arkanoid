#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    game = new Game();


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timer_update()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete game;
}

void MainWindow::timer_update()
{
    game->simulate();
    QImage temp = game->getGameScene();
    ui->label->setPixmap(QPixmap::fromImage(temp));
}

void MainWindow::on_pushButton_clicked()
{
    timer->start(30);
}

void MainWindow::on_pbStop_clicked()
{
    timer->stop();
}
