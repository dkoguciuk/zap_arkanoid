#ifndef GAME_H
#define GAME_H

#include <QImage>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

class Game
{
private:
    cv::Mat playground;
    bool left, up;
    int x,y;
public:
    Game();

    void simulate();
    QImage getGameScene();
};

#endif // GAME_H
