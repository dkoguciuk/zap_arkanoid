#-------------------------------------------------
#
# Project created by QtCreator 2016-05-16T12:26:33
#
#-------------------------------------------------

QT       += core gui

TARGET = zap_arkanoid
TEMPLATE = app

CONFIG+=link_pkgconfig
PKGCONFIG+=opencv

INCLUDEPATH += include

SOURCES += src/main.cpp         \
           src/mainwindow.cpp   \
           src/game.cpp

HEADERS  += include/mainwindow.h    \
            include/game.h

FORMS    += ui/mainwindow.ui
